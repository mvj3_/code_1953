tv=(TextView)findViewById(R.id.TV);
        et=(EditText)findViewById(R.id.ET);
        et.setOnKeyListener(new EditText.OnKeyListener()
        {
        	public boolean onKey(View v,int keyCode,KeyEvent event){
        		tv.setText(et.getText());
        		Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.PHONE_NUMBERS);
        		return false;
        	}
        });